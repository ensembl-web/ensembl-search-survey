from django.apps import AppConfig


class SuvreyConfig(AppConfig):
    name = "survey"
