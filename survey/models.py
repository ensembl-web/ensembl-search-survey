import uuid

from django.db import models
from django.conf import settings
from django_countries.fields import CountryField

SURVEY_CHOICES = list(enumerate(settings.SURVEY_CHOICES))
SURVEY_QUESTION = settings.SURVEY_QUESTION

JOB_CHOICES = (
    (1, "Academic institute/university"),
    (2, "Hospital or medical center"),
    (3, "Commercial"),
    (3, "Government"),
    (4, "Other"),
)
RATE_OF_USAGE_CHOICES = (
    (1, "Daily"),
    (2, "Weekly"),
    (3, "Monthly"),
    (4, "A couple of times a year or less"),
    (5, "Never"),
)


class Survey(models.Model):
    form_id = models.UUIDField(
        default=uuid.uuid4, unique=True
    )
    add_date = models.DateTimeField(auto_now_add=True)
    survey_question = models.CharField(max_length=255)
    survey_answer = models.CharField(max_length=255)
    result = models.JSONField(default={})
    keyword = models.CharField(max_length=255, blank=False)
    user = models.ForeignKey("UserDetails", on_delete=models.CASCADE)


class UserDetails(models.Model):
    add_date = models.DateTimeField(auto_now_add=True)
    country = CountryField(blank_label='(select country)')
    user_agent = models.CharField(blank=True, default='', max_length=300)
    email = models.EmailField(blank=True)
    uuid = models.UUIDField(
        db_index=True, default=uuid.uuid4, unique=True, primary_key=True
    )
    job = models.SmallIntegerField(
        choices=JOB_CHOICES, verbose_name="What kind of organisation do you work for?"
    )
    rate_of_usage = models.SmallIntegerField(
        choices=RATE_OF_USAGE_CHOICES,
        verbose_name="How often do you use Ensembl?",
        blank=True,
    )
    is_usage_gene_info = models.BooleanField(
        default=False, verbose_name="Gene information"
    )
    is_usage_genetic_variation = models.BooleanField(
        default=False, verbose_name="Genetic Variation"
    )
    is_usage_homology = models.BooleanField(
        default=False, verbose_name="Homology between species"
    )
    is_usage_functional_genomic_elements = models.BooleanField(
        default=False,
        verbose_name="Functional genomic elements that regulate expression",
    )
    is_usage_sequences = models.BooleanField(default=False, verbose_name="Sequences")
