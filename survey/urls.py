from django.urls import path

from . import views

app_name = "survey"
urlpatterns = [
    path("", views.get_survey, name="survey"),
    path("save/", views.save, name="save"),
    path("thankyou", views.email_handler, name="thankyou"),
    path("done", views.done, name="done"),
]
