from crispy_forms.helper import FormHelper
from django import forms
from django_countries.widgets import CountrySelectWidget

from django.core.exceptions import ValidationError
from django.shortcuts import get_object_or_404

from .models import (
    UserDetails,
    JOB_CHOICES,
    SURVEY_CHOICES,
    Survey,
    RATE_OF_USAGE_CHOICES,
)


class UserDetailsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(UserDetailsForm, self).__init__(*args, **kwargs)
        self.fields["job"].label = ""
        self.fields["job"].required = True
        self.fields["rate_of_usage"].label = ""
        self.fields["rate_of_usage"].required = True
        self.fields["country"].label = ""
        self.helper = FormHelper()
        self.helper.render_required_fields = True

    job = forms.ChoiceField(choices=JOB_CHOICES, widget=forms.RadioSelect)
    rate_of_usage = forms.ChoiceField(
        choices=RATE_OF_USAGE_CHOICES, widget=forms.RadioSelect
    )

    class Meta:
        model = UserDetails
        fields = [
            "job",
            "country",
            "user_agent",
            "rate_of_usage",
            "is_usage_gene_info",
            "is_usage_genetic_variation",
            "is_usage_homology",
            "is_usage_functional_genomic_elements",
            "is_usage_sequences",
        ]

        widgets = {"job": forms.RadioSelect(), "rate_of_usage": forms.RadioSelect()}


class SurveySearchForm(forms.Form):
    survey_answer = forms.ChoiceField(choices=SURVEY_CHOICES, widget=forms.RadioSelect)

    def __init__(self, *args, **kwargs):
        if "uuid" in kwargs:
            self.uuid = kwargs.pop("uuid")
        super(SurveySearchForm, self).__init__(*args, **kwargs)
        self.fields["survey_answer"].label = ""

    def clean(self):
        """
        clean() is called by form.is_valid()
        Checking if the survey_id exists and matches the request's user
        """
        survey_object = get_object_or_404(Survey, form_id=self.data["survey_id"])
        if str(survey_object.user.uuid) != self.uuid:
            raise ValidationError("Invalid uuid")


class SurveyEmailForm(forms.Form):
    email = forms.EmailField()


