function showGDPRCookieBanner() {
    const cookie_name = "ENSEMBL_PRIVACY_POLICY";
    const cookiesVersion = $.cookie(cookie_name);
    const gdpr_version = "2.0.0";
    const gdpr_policy_url = "https://www.ebi.ac.uk/data-protection/ensembl/privacy-notice";
    const gdpr_terms_url = "https://www.ebi.ac.uk/about/terms-of-use";

    if (!cookiesVersion || (cookiesVersion !== gdpr_version)) {
        console.log('Running cookie');
        $([ "<div class='cookie-message'>",
            "<p class='msg'>",
              "This website requires cookies, and the limited processing of your personal data in order to function. By using the site you are agreeing to this as outlined in our ",
              "<a target='_blank' href='",
              gdpr_policy_url,
              "'>Privacy Policy</a>",
              " and <a target='_blank' href='",
              gdpr_terms_url,
              "'> Terms of Use </a>",
            "</p>",
            "<div class='agree-button'>",
              "<a id='gdpr-agree' class='button no-underline'> I Agree </a>",
            "</div>",
          "</div>"
        ].join(''))
        .appendTo(document.body).show().find('#gdpr-agree').on('click', function (e) {
            $.cookie(cookie_name, gdpr_version, { path: '/', expires: 180 });
            $(this).addClass('clicked')
            .closest('.cookie-message').delay(1000).fadeOut(100);
        });
      return true;
    }
    return false;
}