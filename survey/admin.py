from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin, ExportActionMixin
from import_export.fields import Field

from .models import UserDetails, Survey


class SurveyResource(resources.ModelResource):
    user_job = Field()
    user_rate_of_usage = Field()

    class Meta:
        model = Survey
        fields = (
            "add_date",
            "survey_question",
            "survey_answer",
            "result",
            "keyword",
            "user__add_date",
            "user__user_agent",
            "user__email",
            "user__country",
            "user__is_usage_gene_info",
            "user__is_usage_genetic_variation",
            "user__is_usage_homology",
            "user__is_usage_functional_genomic_elements",
            "user__is_usage_sequences",
        )

    def dehydrate_user_job(self, survey):
        return survey.user.get_job_display()

    def dehydrate_user_rate_of_usage(self, survey):
        return survey.user.get_rate_of_usage_display()


class SurveyAdmin(ExportActionMixin, admin.ModelAdmin):
    resource_class = SurveyResource
    user_details = Field()
    readonly_fields = ("add_date",)
    list_display = ('add_date', 'survey_answer', 'keyword')


class UserDetailsAdmin(admin.ModelAdmin):
    readonly_fields = ("add_date",)
    list_display = ('add_date', 'email')


admin.site.register(UserDetails, UserDetailsAdmin)
admin.site.register(Survey, SurveyAdmin)
