from django.shortcuts import render, redirect

from django.http import HttpResponseForbidden, HttpResponse
from django.utils.html import escape

from survey.forms import SurveySearchForm
from survey.models import Survey, UserDetails, SURVEY_QUESTION, SURVEY_CHOICES
from search.service import EBISearchService



def search(request):
    if not request.session.get("user_session_id", False):
        return redirect(to="/survey")

    return render(request, "search/search.html")


def results(request):
    session_id = request.session.get("user_session_id", False)
    if not session_id:
        return redirect(to="/survey")

    search_term = request.GET.get("keyword")
    if search_term:
        search_service = EBISearchService(search_term)
        search_result = search_service.parse_result()
        survey = Survey(
            user=UserDetails.objects.get(uuid=session_id),
            keyword=search_term,
            result=search_result.toJSON(),
            survey_question=SURVEY_QUESTION,
        )
        survey.save()

        return render(
            request,
            "search/results.html",
            {
                "keyword": search_term,
                "result": search_result,
                "form": SurveySearchForm(),
                "survey_id": survey.form_id,
                "survey_question": SURVEY_QUESTION,
            },
        )


def update(request):
    data = request.POST

    try:
        session_id = request.session.get("user_session_id", False)
        form = SurveySearchForm(data=data, uuid=session_id)

        if form.is_valid():
            survey_object = Survey.objects.get(form_id=data["survey_id"])
            survey_answer_id = int(form.cleaned_data.get("survey_answer"))
            survey_object.survey_answer = dict(SURVEY_CHOICES)[survey_answer_id]
            survey_text = data.get("survey_text", None)
            if survey_text is not None:
                survey_object.survey_answer += " ### " + escape(survey_text.strip())
            survey_object.save()
            return HttpResponse(status=200)
        else:
            return HttpResponseForbidden("No permission")

    except Exception as e:
        print("=========", e)
