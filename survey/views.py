from django.shortcuts import render, redirect

from survey.forms import SurveyEmailForm
from .forms import UserDetailsForm
from .models import UserDetails


def get_survey(request):
    if request.session.get("user_session_id", False):
        return redirect(to="/search")

    validation = bool(request.GET.get("validation_error", False))
    form = UserDetailsForm()
    context = {"form": form, "error": validation}
    return render(request, "survey/index.html", context)


def save(request):
    try:
        data = request.POST.copy()
        data['user_agent'] = request.META['HTTP_USER_AGENT']
        form = UserDetailsForm(data=data)

        if form.is_valid():
            user_details = form.save()
            request.session["user_session_id"] = str(user_details.uuid)
            return redirect(to="/search")
        else:
            return redirect(to="/survey?validation_error=True")
    except Exception as e:
        print(e)


def email_handler(request):
    data = request.POST

    session_id = request.session.get("user_session_id", False)
    try:
        user = UserDetails.objects.get(uuid=session_id)
    except:
        return redirect(to="survey:survey")
    if user.email:
        return redirect(to="survey:done")
    form = SurveyEmailForm(data=data)
    if form.is_valid():
        user.email = form.cleaned_data["email"]
        user.save()
        return redirect(to="survey:done")
    else:
        return render(request, "survey/submit_email.html", context={"form": form})


def done(request):
    return render(request, "survey/thankyou.html")
