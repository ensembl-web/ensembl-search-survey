## Ensembl Search Survey.


### Quick Start
- `$ https://gitlab.ebi.ac.uk/ensembl-web/ensembl-search-survey.git`
- `$ cd ensembl-search-survey`
- `$ sudo docker-compose -f docker-compose.yml up`

To run as a daemon run:
- `$ sudo docker-compose -f docker-compose.yml up -d`

To run unittests:
- `docker-compose run web python manage.py test`

To make migrations and apply in postgresql container:
- `$ sudo docker-compose run web python manage.py makemigrations`
- `$ sudo docker-compose run web python manage.py migrate`

Access to survey page
- `http://0.0.0.0:8000/survey/
 


