import requests
from search.models import SearchResult


class SearchService:
    def __init__(self, keyword):
        self.keyword = keyword

    def get_result(self):
        try:
            result = requests.get(self.endpoint, timeout=10)
        except requests.exceptions.RequestException as err:
            print("API exception: ", err)
            return 500, f"API exception error at {self.endpoint}"

        if result.status_code != 200:
            return 404, f"API non-200 status code at {self.endpoint}"

        return 200, result.json()


class EBISearchService(SearchService):
    def __init__(self, keyword):
        super().__init__(keyword)
        self.search_domain = None

    @property
    def endpoint(self):
        return f"https://www.ebi.ac.uk/ebisearch/ws/rest/{self.search_domain}?query={self.keyword}&fields=name,domain_source,system_name,description&size=10&format=json"

    def parse_result(self):
        self.search_domain = "ensembl"
        vert_result = self.get_result()
        self.search_domain = "ensemblgenomes"
        nonvert_result = self.get_result()
        result = self.mix_results(vert_result, nonvert_result)

        if result[0] == 200:
            total_hits = result[1]["hitCount"]
            top_ten_results = []
            search_fields = ["name", "domain_source", "system_name", "description"]
            for entry in result[1]["entries"]:
                item = {"id": entry["id"]}
                for field in search_fields:
                    field_value = (
                        entry["fields"][field][0]
                        if len(entry["fields"][field]) > 0
                        else ""
                    )
                    item[field] = field_value
                top_ten_results.append(item)
        else:
            total_hits = -1
            top_ten_results = {"err": result[1]}
        return SearchResult(total_hits, top_ten_results)

    @staticmethod
    def mix_results(res1, res2):
        """
        Interleaving V and NV search results to get top 10
        """
        if res1[0] != 200:
            return res1
        if res2[0] != 200:
            return res2

        entries1 = res1[1]["entries"]
        entries2 = res2[1]["entries"]
        entries3 = []
        n1 = len(entries1)
        n2 = len(entries2)
        i = 0

        while i < max(n1, n2) and len(entries3) < 10:
            if i < n1:
                entries3.append(entries1[i])
            if i < n2:
                entries3.append(entries2[i])
            i += 1

        res3 = (
            200,
            {
                "hitCount": res1[1]["hitCount"] + res2[1]["hitCount"],
                "entries": entries3,
            },
        )
        return res3
