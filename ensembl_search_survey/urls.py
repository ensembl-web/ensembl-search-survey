"""ensembl_search_survey URL Configuration

"""
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include

from main import views
urlpatterns = [
    path("", views.get_main, name="main"),
    path("survey/", include("survey.urls", namespace="survey")),
    path("search/", include("search.urls", namespace="search")),

]

urlpatterns += staticfiles_urlpatterns()
if settings.ADMIN_ENABLED:
    urlpatterns += [path('admin/', admin.site.urls), ]
