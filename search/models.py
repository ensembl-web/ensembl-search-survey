import json


class SearchResult:
    def __init__(self, total_hits, top_ten_results):
        self.total_hits = total_hits
        self.top_ten_results = top_ten_results

    def toJSON(self):
        data = {"total_hits": self.total_hits, "top_ten_results": self.top_ten_results}
        return json.dumps(data)
