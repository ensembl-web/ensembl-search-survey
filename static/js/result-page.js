$('input[name="survey_answer"]').click(function() {
    $('#survey_answer_warning').removeClass().addClass('block_hidden');
    $('textarea').addClass('block_hidden');
    if ($(this).val() === $('input[name="survey_answer"]').last().val()) {
        //Last radio button --> Display textarea
        $('textarea').removeClass('block_hidden');
    }
});

$("#main_submit_btn").click(function(){
    let csrfmiddlewaretoken = $('input[name="csrfmiddlewaretoken"]').val();
    let survey_answer = $('input[name="survey_answer"]:checked').val();
    let survey_id = $('#survey_id').val();
    let ajaxURL = $('#feedback_form').attr('ajaxAction');

    if (survey_answer === undefined) {
        $('#survey_answer_warning').removeClass();
        return;
    }
    
    let survey_text;
    if (survey_answer === $('input[name="survey_answer"]').last().val()) {
        survey_text = $('textarea').val();
    }

    $.ajax({url: ajaxURL,
      method: "POST",
      data: {
        csrfmiddlewaretoken: csrfmiddlewaretoken,
        survey_answer: survey_answer,
        survey_id:  survey_id,
        survey_text: survey_text
      },
      success: function(){
        $('input[name="survey_answer"], textarea').prop('disabled', true);
        $('#main_submit_btn, #survey_answer_warning').removeClass().addClass('block_hidden');
        $('#after_submit_btns').fadeIn(600).removeClass().addClass('after_submit_show');
      },
      error: function () {
        $('#survey_answer_warning').removeClass().find('i').text("Error submitting your feedback");
      }
    });
});