from django.test import TestCase
from django.utils.timezone import now

from survey.models import Survey, UserDetails, SURVEY_QUESTION, SURVEY_CHOICES


class UserDetailsTestCase(TestCase):
    def setUp(self):
        self.user_details = UserDetails.objects.create(
            job=1,
            is_usage_gene_info=True,
            is_usage_genetic_variation=True,
            is_usage_sequences=False,
        )
        UserDetails.objects.create(job=3)
        UserDetails.objects.create(job=3)

    def test_user_details_model(self):
        """Animals that can speak are correctly identified"""
        user_details = UserDetails.objects.get(uuid=self.user_details.uuid)
        job_filter = UserDetails.objects.filter(job=3)
        self.assertEqual(user_details.is_usage_gene_info, True)
        self.assertEqual(user_details.is_usage_genetic_variation, True)
        self.assertEqual(user_details.is_usage_homology, False)
        self.assertEqual(user_details.is_usage_functional_genomic_elements, False)
        self.assertEqual(user_details.is_usage_sequences, False)
        self.assertEqual(len(job_filter), 2)
        self.assertLessEqual(user_details.add_date, now())


class SurveyTestCase(TestCase):
    def setUp(self):
        self.user_details = UserDetails.objects.create(
            job=1,
            is_usage_gene_info=True,
            is_usage_genetic_variation=True,
            is_usage_sequences=False,
        )
        self.survey = Survey.objects.create(
            user=self.user_details,
            survey_question=SURVEY_QUESTION,
            survey_answer=SURVEY_CHOICES[3],
        )

    def test_survey_model(self):
        self.assertLessEqual(self.survey.add_date, now())
        self.assertEqual(self.survey.result, {})
        self.assertEqual(type(self.survey.result), dict)
        self.assertEqual(self.survey.keyword, "")
        self.assertEqual(type(self.survey.keyword), str)
